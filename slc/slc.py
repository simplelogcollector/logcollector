import os
import asyncio
from collections import OrderedDict
import docker
import json
from datetime import datetime
import telegram
from slackclient import SlackClient

# client = docker.from_env()
client = docker.DockerClient(base_url='unix://var/run/docker.sock')


# TODO: fix byte to str
def get_logs():
    last_lines = os.environ.get('LINES_N', 3)
    logs = {}
    for container in client.containers.list(all=False):
        print(f' watcher registered for {container.name} {container.image.tags[0]}')
        raw_logs = container.logs(timestamps=True, tail=last_lines).split(b'\n')
        for rline in raw_logs:
            rline_list = str(rline).split(' ', 1)
            if len(rline_list) > 1:
                logs.update(
                    {f'{rline_list[0][2:]}': f'{container.image.tags[0]} aka{container.name} :: {rline_list[1]}'})
    return OrderedDict(sorted(logs.items(), key=lambda t: t[0]))


def send_to_telegram(logs):
    bot = telegram.Bot(token=os.environ.get('TELEGRAM_TOKEN'))
    bot.sendMessage(chat_id=os.environ.get('TELEGRAM_CHAT_ID', '323959662'), text=logs)


def send_to_slack(logs):
    sc = SlackClient(os.environ.get('SLACK_TOKEN'))
    sc.api_call(
        "chat.postMessage",
        channel=os.environ.get('SLACK_CHANNEL', "CEPMVCB9C"),
        text=logs
    )


def process_logs():
    logs = json.dumps(get_logs())
    print('write to disk')
    with open(f'/tmp/logs/x{int(datetime.utcnow().timestamp())}.json', 'w+') as f:
        f.write(logs)

    if os.environ.get('TELEGRAM_TOKEN'):
        print('to telegram')
        send_to_telegram(logs)
    if os.environ.get('SLACK_TOKEN'):
        print('to slack')
        send_to_slack(logs)


async def watcher(name, q, c):
    # loop = asyncio.get_event_loop()
    # await q.join()
    event_filter = os.environ.get('EVENT_FILTER')
    st = container.logs(timestamps=False, tail=1, stream=True)
    while True:
        line = str(st.next())
        if event_filter in line:
            # loop.create_task(get_logs())
            process_logs()


# async def logcollector(name, q, ws):
#     await queue.join()
#
#     # while True:
#     for i in range(2):
#         # Get a "work item" out of the queue.
#         evenet_container = await queue.get()
#
#         print(f'we have event from {evenet_container}')
#         #        TODO: debug time
#         for wch in ws:
#             wch.cancel()
#             # Wait until all worker tasks are cancelled.
#         await asyncio.gather(*ws, return_exceptions=True)
#
#         print('task canceled')
#         # Notify the queue that the "work item" has been processed.
#         q.task_done()

if __name__ == "__main__":
    # execute only if run as a script
    queue = asyncio.Queue()
    watchers = []
    for container in client.containers.list(all=False):
        print(container.name)
        w = asyncio.run(watcher(f'{container.name}', queue, container))
        watchers.append(w)

    # asyncio.run(logcollector('logcollector', queue, watchers))
